import { Component, OnInit } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { NgForm } from '@angular/forms';
import { RecipeServiceService } from './recipe.service.service';
import { Recipe } from './recipe';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit {
  //public recipes: Recipe[];
  //public editRecipe: Recipe;
  //public deleteRecipe: Recipe;

  constructor(
    public deleteRecipe: Recipe,
    public recipes: Recipe[],
    public editRecipe: Recipe,
    private recipeServiceService: RecipeServiceService
  ) {}

  ngOnInit() {
    this.getRecipes();
  }

  public getRecipes(): void {
    this.recipeServiceService.getAllRecipes().subscribe(
      (response: Recipe[]) => {
        this.recipes = response;
        console.log(this.recipes);
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    );
  }

  public getElementId(event:any){
    let element = event.target || event.srcElement || event.currentTarget;
    return element.id;
    
  }

  public onAddRecipe(addForm: NgForm): void {
    
    if(this.getElementId(event) == 'add-recipe-form' ){
      this.recipeServiceService.addRecipe(addForm.value).subscribe(
        (response: Recipe) => {
          console.log(response);
          this.getRecipes();
          addForm.reset();
        },
        (error: HttpErrorResponse) => {
          alert(error.message);
          addForm.reset();
        }
      );
    }
    
  }

  public onUpdateRecipe(recipeId: number, recipeData: any): void {
    this.recipeServiceService.updateRecipe(recipeId, recipeData).subscribe(
      (response: Recipe) => {
        console.log(response);
        this.getRecipes();
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    );
  }

  public onDeleteRecipe(recipeId: number): void {
    this.recipeServiceService.deleteRecipe(recipeId).subscribe(
      (response: void) => {
        console.log(response);
        this.getRecipes();
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    );
  }

  public searchRecipe(key: string): void {
    console.log(key);
    const results: Recipe[] = [];
    for (const recipe of this.recipes) {
      if (
        recipe.name.toLowerCase().indexOf(key.toLowerCase()) !== -1 ||
        recipe.description.toLowerCase().indexOf(key.toLowerCase()) !== -1 ||
        recipe.instructions.toLowerCase().indexOf(key.toLowerCase()) !== -1 ||
        recipe.ingredient ||
        recipe.createdAt
      ) {
        results.push(recipe);
      }
    }
    this.recipes = results;
    if (results.length === 0 || !key) {
      this.getRecipes();
    }
  }

  public onOpenModal(recipe: Recipe, mode: string): void {
    const container = document.getElementById('main-container');
    const button = document.createElement('button');
    button.type = 'button';
    button.style.display = 'none';
    button.setAttribute('data-toggle', 'modal');
    if (mode === 'add') {
      button.setAttribute('data-target', '#addRecipeModal');
    }
    if (mode === 'edit') {
      this.editRecipe = recipe;
      button.setAttribute('data-target', '#updateRecipeModal');
    }
    if (mode === 'delete') {
      this.deleteRecipe = recipe;
      button.setAttribute('data-target', '#deleteRecipeModal');
    }
    container.appendChild(button);
    button.click();
  }
}
