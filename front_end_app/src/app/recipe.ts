export interface Recipe {
    id: number;
    name: string;
    description: string;
    instructions: string;
    ingredient: Array<String>;
    createdAt: Date;
    updatedAt: Date;
  }
  