import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Recipe } from './recipe';
import { environment } from 'src/environments/environment.development';

@Injectable({
  providedIn: 'root'
})
export class RecipeServiceService {
  private apiServerUrl = environment.apiBaseUrl;

  constructor(private http: HttpClient) { }

  public getAllRecipes(): Observable<Recipe[]> {
    return this.http.get<Recipe[]>(`${this.apiServerUrl}/recipe/all`);
  }

  public addRecipe(recipe: Recipe): Observable<Recipe> {
    return this.http.post<Recipe>(`${this.apiServerUrl}/recipe/create`, recipe);
  }

  public updateRecipe(recipeId: number, recipeData: any): Observable<any> {
    return this.http.put<any>(`${this.apiServerUrl}/recipe/update/${recipeId}`, recipeData);
  }

  public deleteRecipe(recipeId: number): Observable<void> {
    return this.http.delete<void>(`${this.apiServerUrl}/recipe/delete/${recipeId}`);
  }

  public deleteAllRecipe(): Observable<void> {
    return this.http.delete<void>(`${this.apiServerUrl}/recipe/delete/all`);
  }
}
