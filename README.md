# recipes_app



## Getting started

To make it easy for you to get started the project, here's a list of recommended next steps.

## About the back end

# Requirements
- [] Nodejs
- [] NPM or YARN

# Installation

- [] Clone the project and type the command npm install to download the project's dependencies.
```
npm install
```
- [] In the db_config.js file found in the config folder, provide all the information related to your database. Mysql is used in this project. 
- [] Get o the terminal and type the command below :
```
node index.js
```
- [] The backend runs on port : 8080, you could verify by using this command :
```
curl localhost:8080
```
- [] Everything is operational here, this is the list of end points you could use to it in postman 
- To sign up or register
```
/api/auth/signup 
```
- To login (don't forget to provide the generated token in the x-access-header of your HTTP request)
```
/api/auth/signin
```
- To demand a refresh token when jwt token is expired
```
api/auth/refreshtoken
```
- To create a recipe (It is preferable to add the ingredient first, there's a many to many mapping between the recipe and the ingredients)
```
api/recipe/create
```
- [] Nevertheless, you could identify the rest of the routes in the routes folder of the project, make sure you provide the right jwt token and also you need to have some authorizations to carry out come activities
