const { authJwt } = require("../midllewares");
const controller = require("../controllers/ingredient_controller.js");

module.exports = function (app) {
    app.use(function (req, res, next) {
      res.header(
        "Access-Control-Allow-Headers",
        "x-access-token, Origin, Content-Type, Accept"
      );
      next();
    });
  
    app.get("/api/ingredient/all", [authJwt.verifyToken], controller.findAll);
  
    app.post(
      "/api/ingredient/create",
      [authJwt.verifyToken],
      authJwt.isAdmin,
      controller.create
    );
  
    app.put(
      "/api/ingredient/update/:id",
      [authJwt.verifyToken],
      authJwt.isAdmin,
      controller.update
    );
  
    app.delete(
      "/api/ingredient/delete/:id",
      [authJwt.verifyToken],
      authJwt.isAdmin,
      controller.delete
    );

  };