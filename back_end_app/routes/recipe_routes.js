const { authJwt } = require("../midllewares");
const controller = require("../controllers/recipe_controller.js");

module.exports = function (app) {
  app.use(function (req, res, next) {
    res.header(
      "Access-Control-Allow-Headers",
      "x-access-token, Origin, Content-Type, Accept"
    );
    next();
  });

  app.get("/api/recipe/all", [authJwt.verifyToken], controller.findAll);

  app.get("/api/recipe/:id", [authJwt.verifyToken], controller.findOne);

  app.post(
    "/api/recipe/create",
    [authJwt.verifyToken],
    authJwt.isAdmin,
    controller.create
  );

  app.put(
    "/api/recipe/update/:id",
    [authJwt.verifyToken],
    authJwt.isAdmin,
    controller.update
  );

  app.delete(
    "/api/recipe/delete/:id",
    [authJwt.verifyToken],
    authJwt.isAdmin,
    controller.delete
  );

  app.delete(
    "/api/recipe/delete/all",
    [authJwt.verifyToken],
    authJwt.isAdmin,
    controller.deleteAll
  );
};
