/**
 * Define the Role model for the database.
 * @function
 * @name defineRoleModel
 * @param {Sequelize} sequelize - The Sequelize instance for the database connection.
 * @param {import('sequelize').DataTypes} Sequelize - The Sequelize DataTypes object.
 * @returns {import('sequelize').ModelCtor<import('sequelize').Model<RoleModelAttributes, any>>} The Role model.
 */

module.exports = (sequelize, Sequelize) => {
  /**
   * @typedef {Object} RoleModelAttributes
   * @property {number} id - The unique identifier of the role (primary key).
   * @property {string} name - The name of the role.
   */
  const Role = sequelize.define("roles", {
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
    },
    name: {
      type: Sequelize.STRING,
    },
  });

  return Role;
};
