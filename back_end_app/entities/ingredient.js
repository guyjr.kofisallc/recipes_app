module.exports = (sequelize, Sequelize) => {
    const Ingredient = sequelize.define("ingredients", {
      id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
      },
      name: {
        type: Sequelize.STRING
      },
    });
  
    return Ingredient;
  };