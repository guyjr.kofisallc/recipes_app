const config = require("../config/db_config.js");

/**
 * The Sequelize instance representing the database connection.
 * @type {import('sequelize').Sequelize}
 */
const Sequelize = require("sequelize");
const sequelize = new Sequelize(config.DB, config.USER, config.PASSWORD, {
  host: config.HOST,
  dialect: config.dialect,
  pool: {
    max: config.pool.max,
    min: config.pool.min,
    acquire: config.pool.acquire,
    idle: config.pool.idle,
  },
});

/**
 * The database object containing Sequelize and the initialized models.
 * @typedef {Object} DB
 * @property {import('sequelize').Sequelize} Sequelize - The Sequelize instance.
 * @property {import('sequelize').Sequelize} sequelize - The Sequelize instance representing the database connection.
 * @property {string[]} ROLES - An array containing role names.
 */

/**
 * The database object containing Sequelize and the initialized models.
 * @type {DB}
 */

const db = {};

db.Sequelize = Sequelize;
db.sequelize = sequelize;

/**
 * The User model representing the "users" table in the database.
 */
db.user = require("../entities/user.js")(sequelize, Sequelize);

/**
 * The Role model representing the "roles" table in the database.
 */
db.role = require("../entities/role.js")(sequelize, Sequelize);

/**
 * The Refresh token model representing the token refresh table in the database.
 */
db.refreshToken = require("../entities/refresh_token.js")(sequelize, Sequelize);

db.recipe = require("../entities/recipe.js")(sequelize, Sequelize);

db.ingredient = require("../entities/ingredient.js")(sequelize, Sequelize);

/**
 * Establishes a many-to-many association between User and Role models.
 */
db.role.belongsToMany(db.user, {
  through: "user_roles",
});

/**
 * Establishes a many-to-many association between Role and User models.
 */
db.user.belongsToMany(db.role, {
  through: "user_roles",
});

db.refreshToken.belongsTo(db.user, {
  foreignKey: "userId",
  targetKey: "id",
});

db.user.hasOne(db.refreshToken, {
  foreignKey: "userId",
  targetKey: "id",
});

db.ingredient.belongsToMany(db.recipe, {
  through: "recipe_ingredients",
});

db.recipe.belongsToMany(db.ingredient, {
  through: "recipe_ingredients",
});

/**
 * An array containing predefined role names.
 * @type {string[]}
 */

db.ROLES = ["user", "admin"];

db.INGREDIENTS = ["onions","fish"]

module.exports = db;
