module.exports = (sequelize, Sequelize) => {
    const Recipe = sequelize.define("recipes", {
      name: {
        type: Sequelize.STRING
      },
      description: {
        type: Sequelize.STRING
      },
      instructions: {
        type: Sequelize.STRING
      }
    });
  
    return Recipe;
  };