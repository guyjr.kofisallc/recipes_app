/**
 * Express application instance
 * @external express
 * @see {@link https://expressjs.com}
 */

/**
 * The express module
 * @type {external:express}
 * @namespace
 */
const express = require("express");

/**
 * Cross-Origin Resource Sharing middleware.
 * @external cors
 * @see {@link https://www.npmjs.com/package/cors}
 */

/**
 * The cors module.
 * @type {external:cors}
 * @namespace
 */
const cors = require("cors");

const db = require("./entities");

const app = express();

/**
 * CORS options for cross-origin resource sharing.
 * @typedef {Object} CorsOptions
 * @property {string} origin - The allowed origin for CORS requests.
 */

/**
 * CORS options configuration.
 * @type {CorsOptions}
 */

var corsOptions = {
  origin: "http://localhost:8081",
};

// Enable CORS middleware with the specified options.
app.use(cors(corsOptions));

// parse requests of content-type - application/json
app.use(express.json());

// parse requests of content-type - application/x-www-form-urlencoded
app.use(express.urlencoded({ extended: true }));

const Role = db.role;

const Ing = db.ingredient;

/**
 * Synchronize the database and create initial data.
 * @function
 * @name syncDatabaseAndInitialize
 * @returns {void}
 */

db.sequelize.sync({ force: true }).then(() => {
  console.log("Drop and Resync Db");
  initial();
});

/**
 * Initialize the database with initial data.
 * @function
 * @name initial
 * @returns {void}
 */
function initial() {
  /**
   * The data representing a role to be created in the database.
   * @typedef {Object} RoleData
   * @property {number} id - The unique identifier of the role.
   * @property {string} name - The name of the role.
   */

  /**
   * The data representing a role to be created in the database.
   * @type {RoleData[]}
   */

  const rolesToCreate = [
    { id: 1, name: "user" },
    { id: 3, name: "admin" },
  ];

  const ingredientsToCreate = [
    { id: 1, name: "onions" },
    { id: 3, name: "fish" },
  ];

  ingredientsToCreate.forEach((roleData) => {
    Ing.create(roleData);
  });

  rolesToCreate.forEach((roleData) => {
    Role.create(roleData);
  });
}

/**
 * Main route handler.
 * @function
 * @name mainRoute
 * @param {Object} req - The request object.
 * @param {Object} res - The response object.
 * @returns {void}
 */

app.get("/", (req, res) => {
  res.json({ message: "Backend running..." });
});

// routes
require("./routes/auth_routes.js")(app);
require('./routes/user_routes.js')(app);
require('./routes/recipe_routes.js')(app);
require('./routes/ingredient_routes.js')(app);

/**
 * The port number where the server listens for incoming requests.
 * @const {number}
 * @default 8080
 */

const PORT = process.env.PORT || 8080;

/**
 * Start the server and listen on the specified port.
 * @function
 * @name startServer
 * @param {number} port - The port number to listen on.
 * @returns {void}
 */

app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}.`);
});
