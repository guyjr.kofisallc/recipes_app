const db = require("../entities");
const ROLES = db.ROLES;
const User = db.user;

/**
 * Middleware function to check for duplicate username or email in the database.
 *
 * @param {Object} req - Express request object.
 * @param {Object} res - Express response object.
 * @param {function} next - Express next middleware function.
 * @returns {void}
 */

checkDuplicateUsernameOrEmail = (req, res, next) => {
  // Check for duplicate username
  User.findOne({
    where: {
      username: req.body.username,
    },
  }).then((user) => {
    if (user) {
      res.status(400).send({
        message: "Failed! Username is already existing!",
      });
      return;
    }

    // Check for duplicate email
    User.findOne({
      where: {
        email: req.body.email,
      },
    }).then((user) => {
      if (user) {
        res.status(400).send({
          message: "Failed! Email is already existing!",
        });
        return;
      }

      next();
    });
  });
};

/**
 * Middleware function to check if the provided roles exist in the system.
 *
 * @param {Object} req - Express request object.
 * @param {Object} res - Express response object.
 * @param {function} next - Express next middleware function.
 * @returns {void}
 */
checkRolesExisted = (req, res, next) => {
  // Check if roles are provided in the request body
  if (req.body.roles) {
    // Loop through the provided roles
    for (let i = 0; i < req.body.roles.length; i++) {
      // Check if the role exists in the predefined list of roles (ROLES)
      if (!ROLES.includes(req.body.roles[i])) {
        // If the role does not exist, send an error response and return from the function
        res.status(400).send({
          message: "Failed! Role does not exist = " + req.body.roles[i],
        });
        return;
      }
    }
  }

  // If all provided roles exist in the system, proceed to the next middleware
  next();
};

/**
 * Object containing middleware functions to verify user sign-up information.
 * @type {VerifySignUp}
 */
const verifySignUp = {
  checkDuplicateUsernameOrEmail: checkDuplicateUsernameOrEmail,
  checkRolesExisted: checkRolesExisted,
};

module.exports = verifySignUp;
