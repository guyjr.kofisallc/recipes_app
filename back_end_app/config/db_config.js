/**
 * Configuration object for the database connection.
 * @typedef {Object} DBConfig
 * @property {string} HOST - The host address of the database (e.g., localhost).
 * @property {string} USER - The database username (e.g., root).
 * @property {string} PASSWORD - The database password for the provided user.
 * @property {string} DB - The name of the database to be used (e.g., recipe_db).
 * @property {string} dialect - The database dialect (e.g., mysql).
 * @property {Object} pool - The database connection pool configuration.
 * @property {number} pool.max - The maximum number of connections in the pool (e.g., 5).
 * @property {number} pool.min - The minimum number of connections in the pool (e.g., 0).
 * @property {number} pool.acquire - The maximum time, in milliseconds, to acquire a connection from the pool (e.g., 30000).
 * @property {number} pool.idle - The maximum time, in milliseconds, that a connection can be idle before being released (e.g., 10000).
 */

module.exports = {
  HOST: "localhost",
  USER: "root",
  PASSWORD: "God$is$grace69",
  DB: "recipe_db",
  dialect: "mysql",
  pool: {
    max: 5,
    min: 0,
    acquire: 30000,
    idle: 10000,
  },
};
